---
title: "Safety Training"
linkTitle: "Safety Training"
weight: 10
description: >
  Let LabSafe handle your Safety Training documentation.
---

Labsafe provides all the necessary tools to record the user safety training users have completed and to monitor and store research project progress.