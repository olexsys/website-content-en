---
date: 2021-11-03
title: "Vanishing atomic form factor derivatives"
linkTitle: "New paper in Acta A"
description: ""
author: OlexSys Ltd.
featured_image: "/images/acta_a.png"

resources:
- src: "**.{png,jpg}"
  title: "Image #:counter"
  params:
    byline: "Images (c) OlexSys Ltd 2020"
---

{{< webp image="/images/acta_a.png">}}

Our latest paper -- *Vanishing of the atomic form factor derivatives in non-spherical structural refinement - a key approximation scrutinized in the case of Hirshfeld atom refinement* -- has just been published in the IUCr Acta A. This is primarily the work of **Laura Midgley**, who is currently working towards her PhD at Durham University with Professor **Norbert Peyerimhoff**.

The paper is [available](https://scripts.iucr.org/cgi-bin/paper?pl5014) directly from the **IUCr** Journals site. You can also request an electronic reprint by contacting us in our [About](/about) section.


## Abstract
When calculating derivatives of structure factors, there is one particular term (the derivatives of the atomic form factors) that will always be zero in the case of tabulated spherical atomic form factors. What happens if the form factors are non-spherical? The assumption that this particular term is very close to zero is generally made in non-spherical refinements (for example, implementations of Hirshfeld atom refinement or transferable aspherical atom models), unless the form factors are refinable parameters (for example multipole modelling). To evaluate this general approximation for one specific method, a numerical differentiation was implemented within the NoSpherA2 framework to calculate the derivatives of the structure factors in a Hirshfeld atom refinement directly as accurately as possible, thus bypassing the approximation altogether. Comparing wR2 factors and atomic parameters, along with their uncertainties from the approximate and numerically differentiating refinements, it turns out that the impact of this approximation on the final crystallographic model is indeed negligible.




