---
date: 2021-08-13
title: "Olex2 workshop at the IUCr2021"
linkTitle: "Olex2 IUCr2021"
description: "We held an online Olex2 Workshop at the IUCr 2021 on 13th of August 2021"
author: OlexSys Ltd.
featured_image: "/images/iucr2021.jpg"
resources:
- src: "**.{png,jpg}"
  title: "Image #:counter"
  params:
    byline: "Images (c) OlexSys Ltd 2021"
---

{{< webp image="/images/multi_people.jpg" width="78%">}}

We held a vitrual Olex2 workshop at the 25th IUCr Congress in Prague 2021 on **13th of August 2021** from 9am to 4pm CEST. There were 25 participants and we had five Olex2 people available during the day. We thank the organisers of the IUCr Congress for handling the registrations.

We also thank all participants for creating a lively and informative day with us!