
---
title: "Changelog: Olex2-1.3"
linkTitle: "Changelog"
date: 2020-06-18
type: blog
description: >
  We will keep adding a note of changes we have made
---

In the next version of Olex2 there will be the following changes (among others):



### Fractional entry for Z'

The Z' input boxes will now take either an integer, a decimal value or a **fraction**. Instead of writing **0.125**, you can now write **1/8**, for example