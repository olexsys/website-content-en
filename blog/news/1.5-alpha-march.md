---
date: 2021-03-12
title: "Olex2-1.5-alpha"
linkTitle: "1.5-alpha"
description: "We have just updated the 1.5-alpha release!"
author: OlexSys Ltd.
featured_image: "/images/1.5-alpha.png"

resources:
- src: "**.{png,jpg}"
  title: "Image #:counter"
  params:
    byline: "Images (c) OlexSys Ltd 2020"
---

{{< webp image="/images/1.5-alpha.png">}}

We are nearing our first full release of the new version of Olex2: the 1.5! This version sees a change from Python 2.7 to Python 3.8 -- plus there is a large number of small improvements and bug fixes.

The old Olex2-1.3 line will no longer be developed and will only receive service updates where necessary.

You are welcome to download and install this new version (just get the normal **[installer](/olex2/docs/getting-started/installing-olex2/)** and choose **Olex2-1.5-alpha** from the drop-down menu.

And remember: each installation of Olex2 is completely independent of any other version -- so installing this new version will not affect your existing installations at all.

