---
date: 2021-01-14
title: "OlexSys Calendar 2021"
linkTitle: "Calendar 2021"
description: "The OlexSys Calender 2021"
author: OlexSys Ltd.
featured_image: "/images/Cal2021.jpg"
resources:
- src: "**.{png,jpg}"
  title: "Image #:counter"
  params:
    byline: "Images (c) OlexSys Ltd 2021"
---

{{< webp image="/images/rainbow.jpg" alt="">}}

If you would like an electronic copy of our 2021 OlexSys Calendar, please drop us a line at helpdesk@olexsys.org or use our contact form in the [About Section](/about/) section of this website.


Let us know if you would like to receive a hard-copy version of the 2022 calendar next year -- we will send one free of charge to the first 30 people to contact us with **Calendar2022** in the subject line!
