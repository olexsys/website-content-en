---
date: 2024-10-24
title: "Sagamore 2024"
linkTitle: "Sagamore 2024"
description: "There will be an Olex2 workshop at the Sagamore 2024 meeting in November."
author: OlexSys Ltd.
featured_image: "/images/sagamore2024.jpg"

resources:
- src: "**.{png,jpg}"
  title: "Image #:counter"
  params:
    byline: "Images (c) OlexSys Ltd 2020"
---

{{< webp image="/images/sagamore2024.jpg">}}

There will be an Olex2 workshop at the Sagamore 2024 meeting in New Delhi in November 2024.

Horst Puschmann, Florian Kleemiss and Niklas Ruth will present the Olex2, NoSpherA2 and QCrBox on November, 15 (Saturday) as part of the [Sagamore XX](https://www.snu.edu.in/conferences/Sagamore2024/) conference.

