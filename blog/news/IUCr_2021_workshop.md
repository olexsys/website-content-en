---
date: 2021-07-05
title: "Olex2-IUCr2021"
linkTitle: "Olex2-IUCr2021"
description: "13th of August 2021: online Olex2 Workshop at the IUCr 2021"
author: OlexSys Ltd.
featured_image: "/images/iucr2021.jpg"
resources:
- src: "**.{png,jpg}"
  title: "Image #:counter"
  params:
    byline: "Images (c) OlexSys Ltd 2021"
---

{{< webp image="/images/multi_people.jpg" width="78%">}}

Welcome to the Olex2 workshop at the 25th IUCr Congress in Prague 2021, which will be held online on the **13th of August 2021** from 9am to 4pm CEST.

We will take a maximum of 35 participants, and there will be four instructors available during the day. The Registration will be handled by the official [IUCr Registration website](https://iucr25.org/workshops/).

Please don’t hesitate and talk to us with any questions you may have: info@olexsys.org.

This workshop is suitable for all levels of experience. We will cover the following areas in an informal and friendly setting:

- Structure Solution
- Structure Refinement & Model Building
- Restraints & Constraints
- Working with Disordered Structures
- CIF file Generation in Olex2
- Finishing Structures for Publication

Before you attend the workshop, you may want to have a look at some of our videos on our [You Tube Channel](https://www.youtube.com/channel/UCV6B2W8zlmXqkU2DbIviQow/videos?view_as=subscriber).

There are plenty of videos -- from getting started with Olex2 through to explaining more complicated issues like disorder modelling across symmetry elements.

Please install this latest version of Olex2 if you are planning to attend this workshop. (just get the normal **[installer](/olex2/docs/getting-started/installing-olex2/)** and choose **Olex2-1.5** from the drop-down menu).

### 9.00 am: Getting Started with Olex2 / Fundamental Basics
Here you can learn how to set up everything around Olex2. If you haven't already, please install the latest version of Olex2. Please be sure to also keep any external software, such as the ShelX programs ready. Please prepare the  newest version 1.5 of Olex2 before the workshop here: https://www.olexsys.org/olex2/docs/getting-started/installing-olex2/ We will also discuss the general features and how to use Olex2, if you are entirely new to this program. If you are already very familiar with Olex2, you can skip this unit!

### 11.00 am: Tips & Tricks around Olex2
In this part we will take you through the general and mostly used commands, how you can use Olex2 to check your data and model and which ‘good habits can increase your workflow in Olex2.|

### 12.00 am: Working Lunch
Have a bite to eat or continue chatting to us or other participants on this zoom meeting!

### 12:30pm: Modelling Disorder and Masking a Solvent
The two most common problems in real crystal structures will be discussed in this section. We will show you how you can spot disorders and how to deal with them efficiently in Olex2. Also, we will talk about when and when not to use the solvent masking tool.

### 13.30: NoSpherA2 - Non Spherical Atoms in Olex2
Besides developing and improving Olex2, we also try to improve the general methods used in crystallography. NoSpherA2 is a brand new feature that makes the use of quantum mechanically based form factors feasible for general usage. We will discuss the background and show you by an example, how the use of calculated, non-spherical form factors benefit a structure.

### 14.30pm: Time for your questions! This is the best part!
The title says it all! If you have any remaining questions, feel free to ask them right here.

### 16.00pm: Close


We are looking forward to this Olex2 workshop. See you in virtual Prague!

{{< webp image="/images/mouse.jpg" width="200px">}}

If you are attending, please make sure you are using a dual monitor set-up and have a computer mouse at hand. We would also really appreciate if you could kindly leave your camera on throughout the workshop -- it is so much easier talking to people than to black rectangles!


