---
date: 2020-10-22
title: "New OlexSys Website"
linkTitle: "New Website"
description: "A new website with integrated Olex2 documentation."
author: OlexSys Ltd.
featured_image: "/images/new-website.jpg"
---

If you are seeing this, then you have found our new website! We are currently trialling this site -- plese give us some feedback!

{{< webp image="/images/new-website.jpg">}}

In generating this site, the focus was on maintainability -- and hence this entire site is build from a whole bunch of simple markdown files. If **you** are interested in contributing to the documentation, please let us know!

