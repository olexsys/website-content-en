---
date: 2020-09-08
title: "Olex2-1.3: 1st anniversary!"
linkTitle: "One Year of Olex2 1.3"
description: "Olex2-1.3 should now be the only version of Olex2 anyone is using."
author: OlexSys Ltd.
featured_image: "/images/news13.png"
---

{{< webp image="/images/news13.png" >}}


About a year ago, we relased v. 1.3 of Olex2. By now, this is the version that everyone can safely use. So: if your splash screen doesn't look like this, then it's time to update your version of Olex2 to this latest version.

>CRYST By the way, your existing versions of Olex2 will not be affected by this update at all, and will just keep working! More about versions can be found [here](/olex2/docs/getting-started/versions/)


