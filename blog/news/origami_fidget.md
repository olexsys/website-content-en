---
date: 2023-08-14
title: "Olex2 Origami Fidget"
linkTitle: "Origami Fidget"
description: "Instructions for how to fold the Olex2 Origami Fidget Spinner"
author: Horst Puschmann
resources:
- src: "**.{png,jpg}"
  title: "Image #:counter"
  params:
    byline: "Fold you own!"
---

Here are the instructions of how to fold the Olex2 Origami Fidget Spinner.

If you are at the IUCr in Melbourne, come and find one of us to get your Fidget Spinner. If not, you can {{< a href="http://www.olex2.org/adverts/fidget_olex2.png" download="fidget.png" >}} download the design {{< /a >}} and print it out for yourself. You need to carfully trim this after printing!

{{< youtube id="gZykxq_91ME" >}}

If you would like your own customised Fidget Spinner, please send us an e-mail with up to three images in square design, and we will make one for you to download.
