---
title: "Experimantal Protocols"
linkTitle: "Experimantal Protocols"
weight: 5
description: >
  Generate experimental protocols and manged your staff safely
---

**Experiment planning** is a tool that is in particular useful for experiments running overnight - the user can specify emergency contacts, what to do with the experiment in the case of an emergency and what central supplies (e.g. water, power) affect the experiment. This tool also is useful for the stock control since when running the experiment the chemical stock is updated.

For experiments that must be performed according to protocols - the online protocols can be used as an experiment template. This saves time on COSHH assessment and provides information on how the particular experiment should be executed. This tool is in particular useful for students or visitors to the laboratory.