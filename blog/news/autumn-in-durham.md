---
date: 2020-10-22
title: "Autumn in Durham"
linkTitle: "Autumn"
description: "Once again, the leaves are changing colour"
author: OlexSys Ltd.
featured_image: "/images/autumn.jpg"

resources:
- src: "**.{png,jpg}"
  title: "Image #:counter"
  params:
    byline: "Images (c) OlexSys Ltd 2020"
---

The days are getting shorter, and the leaves are turning very colourful at this time of the year in Durham.

{{< webp image="/images/autumn.jpg" alt="Autumn colours at the riverbanks in Durham">}}

We are busy developing this new website, a new Support System for Olex2 and are also making the first inroads into the next release: Olex2-1.5, which will be using Python 3.8.

We have also been very busy with getting our **NoSpherA2** paper published -- and it's looking good! 